<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 18:12
 */

class TestLoaderAdapterTest extends KLoaderAdapterLibrary
{
    /**
     * The adapter type
     *
     * @var string
     */
    protected $_type = 'test';

    /**
     * The class prefix
     *
     * @var string
     */
    protected $_prefix = 'Test';
}