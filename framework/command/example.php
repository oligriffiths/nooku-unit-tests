<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 26/02/2013
 * Time: 15:21
 */

class TestCommandExample extends KCommand implements KObjectHandlable
{
    /**
     * Example method, always returns false for testing
     *
     * @param KCommandContext $context
     * @return bool
     */
    protected function _example(KCommandContext $context)
    {
        return false;
    }

    /**
     * Returns the object handle
     * @return string
     */
    public function getHandle()
    {
        return 'example';
    }
}