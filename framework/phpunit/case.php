<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 27/02/2013
 * Time: 18:14
 */

class TestPhpunitCase extends PHPUnit_Framework_TestCase
{
    public function getService($identifier, $options = array())
    {
        return KServiceManager::get($identifier, $options);
    }


    public function getIdentifier($identifier)
    {
        return KServiceManager::getIdentifier($identifier);
    }
}