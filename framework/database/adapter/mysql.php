<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 18:40
 */

class TestDatabaseAdapterMysql extends KDatabaseAdapterMysql
{
    /**
     * Get the database name
     *
     * @return string    The database name
     */
    public function getDatabase()
    {
        return 'database_name';
    }

    /**
     * Set the database name
     *
     * @param     string     The database name
     * @return  KDatabaseAdapterAbstract
     */
    public function setDatabase($database)
    {

    }

    /**
     * Connect to the db
     *
     * @return  KDatabaseAdapterAbstract
     */
    public function connect()
    {
        return $this;
    }

    /**
     * Turns off autocommit mode
     *
     * @return  boolean  Returns TRUE on success or FALSE on failure.
     */
    public function begin()
    {
        return true;
    }

    /**
     * Commits a transaction
     *
     * @return  boolean  Returns TRUE on success or FALSE on failure.
     */
    public function commit()
    {
        return true;
    }

    /**
     * Rolls back a transaction
     *
     * @return  boolean  Returns TRUE on success or FALSE on failure.
     */
    public function rollback()
    {
        return true;
    }

    /**
     * Lock a table
     *
     * @param   string $table  The name of the table.
     * @return  boolean  TRUE on success, FALSE otherwise.
     */
    public function lock($table)
    {
        return true;
    }

    /**
     * Unlock tables
     *
     * @return  boolean  TRUE on success, FALSE otherwise.
     */
    public function unlock()
    {
        return true;
    }

    /**
     * Determines if the connection to the server is active.
     *
     * @return      boolean
     */
    public function isConnected()
    {
        return true;
    }

    /**
     * Checks if inside a transaction
     *
     * @return  boolean  Returns TRUE if a transaction is currently active, and FALSE if not.
     */
    public function inTransaction()
    {
        return true;
    }

    public function execute($query, $mode = KDatabase::RESULT_STORE)
    {
        $this->_affected_rows = 1;
        $this->_insert_id = 1;

        return true;
    }

    public function getTableSchema($name)
    {
        $table              = $this->getService('lib:database.schema.table');
        $table->name        = $name;
        $table->engine      = 'InnoDB';
        $table->type        = 'BASE';
        $table->length      = '16384';
        $table->autoinc     = '2';
        $table->collation   = 'utf8_general_ci';
        $table->behaviors   = array();
        $table->description = '';

        return $table;
    }

    /**
     * Fetch the first field of the first row
     *
     * @param   mysqli_result   The result object. A result set identifier returned by the select() function
     * @param   integer         The index to use
     * @return The value returned in the query or null if the query failed.
     */
    protected function _fetchField($result, $key = 0)
    {
        return 'john';
    }

    /**
     * Fetch an array of single field results
     *
     * @param   mysqli_result   The result object. A result set identifier returned by the select() function
     * @param   integer         The index to use
     * @return  array           A sequential array of returned rows.
     */
    protected function _fetchFieldList($result, $key = 0)
    {
        return array('john','steve','greg','oli');
    }

    /**
     * Fetch the first row of a result set as an associative array
     *
     * @param   mysqli_result   The result object. A result set identifier returned by the select() function
     * @return array
     */
    protected function _fetchArray($sql)
    {
        return array(
            'article_id' => '1',
            'title' => 'Article title'
        );
    }

    /**
     * Fetch all result rows of a result set as an array of associative arrays
     *
     * If <var>key</var> is not empty then the returned array is indexed by the value
     * of the database key.  Returns <var>null</var> if the query fails.
     *
     * @param   mysqli_result   The result object. A result set identifier returned by the select() function
     * @param   string          The column name of the index to use
     * @return  array   If key is empty as sequential list of returned records.
     */
    protected function _fetchArrayList($result, $key = '')
    {
        return array(
            array(
                'article_id' => '1',
                'title' => 'Article title'
            )
        );
    }

    /**
     * Fetch the first row of a result set as an object
     *
     * @param   mysqli_result  The result object. A result set identifier returned by the select() function
     * @param object
     */
    protected function _fetchObject($result)
    {
        return
            (object) array(
                'article_id' => '1',
                'title' => 'Article title'
            );
    }

    /**
     * Fetch all rows of a result set as an array of objects
     *
     * If <var>key</var> is not empty then the returned array is indexed by the value
     * of the database key.  Returns <var>null</var> if the query fails.
     *
     * @param   mysqli_result  The result object. A result set identifier returned by the select() function
     * @param   string         The column name of the index to use
     * @return  array   If <var>key</var> is empty as sequential array of returned rows.
     */
    protected function _fetchObjectList($result, $key = '')
    {
        return array(
            (object) array(
                'article_id' => '1',
                'title' => 'Article title'
            )
        );
    }

    /*
     * Surpressed functions
     */
    protected function _quoteValue($value){}
}