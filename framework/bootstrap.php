<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 16:41
 */

error_reporting(E_ALL);
ini_set('display_errors','On');

$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['SERVER_PORT'] = '80';

// Set flag that this is a parent file
define( '_JEXEC', 1 );

define('JPATH_ROOT',        dirname(dirname(dirname(__FILE__))));
define('JPATH_APPLICATION'  , JPATH_ROOT.'/application/site');
define('JPATH_BASE'         , JPATH_APPLICATION );

define('JPATH_VENDOR'       , JPATH_ROOT.'/vendor' );
define('JPATH_SITES'        , JPATH_ROOT.'/sites');

define( 'DS', DIRECTORY_SEPARATOR );

require_once(JPATH_APPLICATION.'/public/bootstrap.php' );

KServiceManager::get('loader')->loadIdentifier('com://site/application.aliases');

/**
 * Load in testing framework
 */
require_once dirname(__FILE__) . '/loader/adapter/test.php';

//Register test locator and adapter
KServiceManager::get('loader')->addAdapter(new TestLoaderAdapterTest(array('basepath' => dirname(__FILE__))));
$locator = new TestServiceLocatorTest(new KConfig(array('service_manager' => KServiceManager::getInstance(), 'service_identifier' => 'lib:service.locator.test')));
KServiceIdentifier::addLocator($locator);

KServiceManager::get('loader')->loadIdentifier('com://site/application.aliases');

$context = new KCommandContext();
KServiceManager::get('application')->registerCallback('before.run', function(){
    return false;
});

KServiceManager::get('application')->removeEventListener('onException', array(KServiceManager::get('application'), 'error'));

//Run application
KServiceManager::get('application')->run();