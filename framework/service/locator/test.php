<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 18:14
 */

class TestServiceLocatorTest extends KServiceLocatorLibrary
{
    /**
     * The type
     *
     * @var string
     */
    protected $_type = 'test';

    /**
     * Get the classname
     *
     * @param 	mixed  		 An identifier object - lib:[path].name
     * @return string|false  Return object on success, returns FALSE on failure
     */
    public function findClass(KServiceIdentifier $identifier)
    {
        $classes   = array();
        $classname = 'Test'.ucfirst($identifier->package).KInflector::implode($identifier->path).ucfirst($identifier->name);

        if (!class_exists($classname))
        {
            //Add the classname to prevent re-look up
            $classes[] = $classname;

            // use default class instead
            $classname = 'Test'.ucfirst($identifier->package).KInflector::implode($identifier->path).'Default';

            if (class_exists($classname)) {
                $classname = false;
            } else {
                $classes[] = $classname;
            }
        }
        else $classes[] = $classname;

        return $classname;
    }
}