<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 14:10
 */

class KControllerModelTest extends TestPhpunitCase
{
    protected $_object;

    public function setUp()
    {
        $this->getService('loader')->loadIdentifier('com://site/articles.aliases');

        //Create controller
        $this->_object = $this->getService('com://site/articles.controller.article');

        $permission = $this->_object->getBehavior('article');

        //Remove permission object so we can do add/edit/delete
        $this->_object->getCommandChain()->dequeue($permission);

        //Ensure controller persists
        KServiceManager::set('com://site/articles.controller.article', $this->_object);
    }


    protected function tearDown()
    {
       unset($this->_object);
    }


    public function testGetModel()
    {
        $model = $this->_object->getModel();

        //Model must always implement KModelInterface
        $this->assertInstanceOf('KModelInterface', $model);
    }


    public function testGetView()
    {
        $view = $this->_object->getView();

        //View must always implement KViewInterface
        $this->assertInstanceOf('KViewInterface', $view);
    }


    public function testBrowse()
    {
        $rowset = $this->_object->browse();

        //Browse must always return object implemeting KDatabaseRowsetInterface
        $this->assertInstanceOf('KDatabaseRowsetInterface', $rowset);
    }


    public function testRead()
    {
        $row = $this->_object->read();

        //Read must always return object implemeting KDatabaseRowInterface
        $this->assertInstanceOf('KDatabaseRowInterface', $row);
    }


    public function testAdd()
    {
        $context = $this->_object->getCommandContext();
        $context->request->data->add(array('title' => 'Article title'));
        $row = $this->_object->add($context);

        //Ensure a row was created
        $this->assertInstanceOf('KDatabaseRowInterface', $row);

        //Ensure status code is correct
        $this->assertTrue($context->response->getStatusCode() == KControllerAbstract::STATUS_CREATED);

        //Set the ID state
        $this->_object->id($row->id);
    }


    public function testEdit()
    {
        $context = $this->_object->getCommandContext();
        $context->request->setData(array('title' => 'Article title edited'));
        $row = $this->_object->edit($context);

        //Ensure a row was edited
        $this->assertInstanceOf('KDatabaseRowInterface', $row);

        //Ensure status code is correct
        $this->assertTrue($context->response->getStatusCode() == KControllerAbstract::STATUS_RESET);
    }


    public function testDelete()
    {
        $context = $this->_object->getCommandContext();
        $row = $this->_object->delete($context);

        //Ensure a row is returned
        $this->assertInstanceOf('KDatabaseRowInterface', $row);

        //Ensure the status code is correct
        $this->assertTrue($context->response->getStatusCode() == KControllerAbstract::STATUS_UNCHANGED);
    }
}