<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 14:10
 */

class KConfigTest extends TestPhpunitCase
{
    protected $_config;
    protected $_data;

    protected function setUp()
    {
        $this->_data = array(
            'var1' => 10,
            'var2' => 20,
            'var3' => 30
        );

        $this->_config = new KConfig($this->_data);
    }


    /**
     * Retrieve a configuration item and return $default if there is no element set.
     */
    public function testGet()
    {
        $this->assertTrue($this->_config->get('var1') == 10);
    }

    /**
     * Set a configuration item
     */
    public function testSet()
    {
        $this->_config->set('var2', 40);

        $this->assertTrue($this->_config->var2 == 40);
    }

    /**
     * Check if a configuration item exists
     */
    public function testHas()
    {
        $this->assertTrue($this->_config->has('var2'));
    }

    /**
     * Remove a configuration item
     */
    public function testRemove()
    {
        $this->_config->remove('var2');

        $this->assertFalse($this->_config->has('var2'));
    }

    /**
     * Append values
     */
    public function testAppend()
    {
        $data = array(
            'var1',
            'var2' => array(
                'var2.1' => 1
            )
        );
        $config = new KConfig();
        $config->append($data);

        $this->assertEquals($config->toArray(), $data);
    }

    /**
     * Unbox a KConfig object
     */
    public function testUnbox()
    {
        $this->assertEquals(KConfig::unbox($this->_config), $this->_data);
    }

    /**
     * Return an associative array of the config data.
     */
    public function testToArray()
    {
        $this->assertEquals($this->_config->toArray(), $this->_data);
    }

    /**
     * Returns a string with the encapsulated data in JSON format
     */
    public function testToString()
    {
        $this->assertEquals($this->_config->toString(), json_encode($this->_data));
    }
}