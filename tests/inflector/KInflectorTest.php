<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 14:10
 */

class KInflectorTest extends TestPhpunitCase
{
    /**
     * Provides data for the test
     */
    public static function provideNames()
    {
        // $camelized, $underscored, $exploded, $humanized, $tableized, $classified, $parts
        return array(
            array(
                'PrefixBaseSuffix',
                'prefix_base_suffix',
                array('prefix', 'base', 'suffix'),
                'Prefix Base Suffix',
                'prefix_base_suffices',
                'PrefixBaseSuffix',
                array(1, 'base')
            ),
            array(
                'BaseSuffix',
                'base_suffix',
                array('base', 'suffix'),
                'Base Suffix',
                'base_suffices',
                'BaseSuffix',
                array(1, 'suffix')
            ),
            array(
                'PrefixBase',
                'prefix_base',
                array('prefix', 'base'),
                'Prefix Base',
                'prefix_bases',
                'PrefixBasis', //@TODO: Not sure this is correct, should bases singularize to base or basis?
                array(0, 'prefix')
            ),
            array(
                'Base',
                'base',
                array('base'),
                'Base',
                'bases',
                'Basis',  //@TODO: Not sure this is correct, should bases singularize to base or basis?
                array(0, 'base')
            ),
            array(
                'PrefixDoubleBaseSuffix',
                'prefix_double_base_suffix',
                array('prefix', 'double', 'base', 'suffix'),
                'Prefix Double Base Suffix',
                'prefix_double_base_suffices',
                'PrefixDoubleBaseSuffix',
                array(2, 'base')
            )
        );
    }

    /**
     * Provides data for the test
     */
    public static function providePlurals()
    {
        // singular, plural
        return array(
            array('person',     'people'),
            array('item',       'items'),
            array('aircraft',   'aircraft'),
            array('cannon',     'cannon'),
            array('deer',       'deer'),
            array('quiz',       'quizzes'),
            array('child',      'children'),
            array('foot',       'feet'),
            array('suffix',     'suffices'),
            array('dish',       'dishes'),
            array('tomato',     'tomatoes'),
            array('hero',       'heroes'),
            array('cherry',     'cherries'),
            array('monkey',     'monkeys'),
            array('calf',       'calves'),
            array('knife',      'knives'),
            array('moose',      'moose'),
            array('swine',      'swine'),
            array('woman',      'women'),
            array('alumna',     'alumnae'),
            array('vertex',     'vertices'),
            array('crisis',     'crises'),
            array('addendum',   'addenda'),
            array('genus',      'genera')
        );
    }

    /**
     * Returns given word as CamelCased
     *
     * Converts a word like "foo_bar" or "foo bar" to "FooBar". It
     * will remove non alphanumeric characters from the word, so
     * "who's online" will be converted to "WhoSOnline"
     *
     * @dataProvider provideNames
     */
    public function testCamelize($camelized, $underscored)
    {
        $this->assertEquals(KInflector::underscore($camelized), $underscored);
    }

    /**
     * Converts a word "into_it_s_underscored_version"
     *
     * Convert any "CamelCased" or "ordinary Word" into an "underscored_word".
     *
     * @dataProvider provideNames
     */
    public function testUnderscore($camelized, $underscored)
    {
        $this->assertEquals(KInflector::camelize($underscored), $camelized);
    }

    /**
     * Convert any "CamelCased" word into an array of strings
     *
     * Returns an array of strings each of which is a substring of string formed
     * by splitting it at the camelcased letters.
     *
     * @dataProvider provideNames
     */
    public function testExplode($camelized, $underscored, $exploded)
    {
        $this->assertEquals( KInflector::explode($camelized), $exploded);
    }

    /**
     * Convert  an array of strings into a "CamelCased" word
     *
     * @dataProvider provideNames
     */
    public function implode($camelized, $underscored, $exploded)
    {
        $this->assertEquals( KInflector::implode($exploded), $camelized);
    }

    /**
     * Returns a human-readable string from $word
     *
     * Returns a human-readable string from $word, by replacing
     * underscores with a space, and by upper-casing the initial
     * character by default.
     *
     * @dataProvider provideNames
     */
    public function testHumanize($camelized, $underscored, $exploded, $humanized)
    {
        $this->assertEquals( KInflector::humanize($underscored), $humanized);
    }

    /**
     * Converts a class name to its table name according to Koowa
     * naming conventions.
     *
     * Converts "Person" to "people"
     *
     * @dataProvider provideNames
     */
    public function testTableize($camelized, $underscored, $exploded, $humanized, $tableized)
    {
        $this->assertEquals( KInflector::tableize($camelized), $tableized);
    }

    /**
     * Converts a table name to its class name according to Koowa
     * naming conventions.
     *
     * Converts "people" to "Person"
     *
     * @dataProvider provideNames
     */
    public function testClassify($camelized, $underscored, $exploded, $humanized, $tableized, $classified)
    {
        $this->assertEquals( KInflector::classify($tableized), $classified);
    }

    /**
     * Returns camelBacked version of a string.
     *
     * Same as camelize but first char is lowercased
     *
     * @dataProvider provideNames
     */
    public function testVariablize($camelized, $underscored, $exploded, $humanized, $tableized)
    {
        $this->assertEquals( KInflector::variablize($underscored), lcfirst($camelized));
    }

    /**
     * Gets a part of a CamelCased word by index
     *
     * Use a negative index to start at the last part of the word (-1 is the
     * last part)
     *
     * @dataProvider provideNames
     */
    public function testgetPart($camelized, $underscored, $exploded, $humanized, $tableized, $classified, $parts)
    {
        $this->assertEquals( KInflector::getPart($camelized, $parts[0]), $parts[1]);
    }

    /**
     * @dataProvider providePlurals
     */
    public function testIsPlural($singular, $plural)
    {
        $this->assertTrue(KInflector::isPlural($plural));
    }

    /**
     * @dataProvider providePlurals
     */
    public function testIsSingular($singular, $plural)
    {
        $this->assertTrue(KInflector::isSingular($singular));
    }

    /**
     * @dataProvider providePlurals
     */
    public function testPluralize($singular, $plural)
    {
        $this->assertEquals(KInflector::pluralize($singular), $plural);
    }

    /**
     * @dataProvider providePlurals
     */
    public function testSingularize($singular, $plural)
    {
        $this->assertEquals(KInflector::singularize($plural), $singular);
    }
}


