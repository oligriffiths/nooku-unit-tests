<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 26/02/2013
 * Time: 15:16
 */

class KCommandEventTest extends TestPhpunitCase
{
    /**
     * Generic Command handler
     *
     * @param 	string 	The command name
     * @param 	object  The command context
     * @return	boolean
     */
    public function testExecute()
    {
        $command = $this->getService('lib:command.event', array('event_dispatcher' => 'lib:event.dispatcher.default'));

        $context = new KCommandContext();
        $context->setSubject($this->getService('lib:object'));
        $this->assertTrue($command->execute('event', $context));
    }

    /**
     * Get the event dispatcher
     *
     * @return  KEventDispatcher
     */
    public function testGetEventDispatcher()
    {
        $command = $this->getService('lib:command.event', array('event_dispatcher' => 'lib:event.dispatcher.default'));

        $this->assertInstanceOf('KEventDispatcherInterface', $command->getEventDispatcher());
    }
}
