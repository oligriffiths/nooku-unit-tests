<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 26/02/2013
 * Time: 15:16
 */

class KCommandChainTest extends TestPhpunitCase
{
    protected $_chain;
    protected $_command;
    
    protected function setUp()
    {
        $this->_command = $this->getService('test:command.example');
        $this->_chain = $this->getService('lib:command.chain');
        $this->_chain->enqueue($this->_command);
    }


    protected function tearDown()
    {
        unset($this->_chain);
        unset($this->_command);
    }


    /**
     * Attach a command to the chain
     *
     * The priority parameter can be used to override the command priority while enqueueing the command.
     *
     * @param   KCommandInterface   $this->_command
     * @param   integer             $priority The command priority, usually between 1 (high priority) and 5 (lowest),
     *                                        default is 3. If no priority is set, the command priority will be used
     *                                        instead.
     * @return KCommandChain
     * @throws \InvalidArgumentException if the object doesn't implement KCommandInterface
     */
    public function testEnqueue()
    {
        $this->assertTrue($this->_chain->enqueue($this->_command));
    }

    /**
     * Removes a command from the queue
     *
     * @param   KCommandInterface $object
     * @return  boolean    TRUE on success FALSE on failure
     * @throws  \InvalidArgumentException if the object implement KCommandInterface
     */
    public function testDequeue()
    {
        $this->assertTrue($this->_chain->dequeue($this->_command));
    }

    /**
     * Check if the queue does contain a given object
     *
     * @param  KCommandInterface $object
     * @return bool
     * @throws  \InvalidArgumentException if the object implement KCommandInterface
     */
    public function testContains()
    {
        $this->assertTrue($this->_chain->contains($this->_command));
    }

    /**
     * Run the commands in the chain
     *
     * If a command returns the 'break condition' the executing is halted.
     *
     * @param   string          $name
     * @param   KCommandContext $context
     * @return  void|boolean    If the chain breaks, returns the break condition. Default returns void.
     */
    public function testRun()
    {
        $context = new KCommandContext();

        //Ensure empty chain
        $this->_chain = $this->getService('lib:command.chain');

        //Ensures that an empty chain returns null
        $this->assertNull($this->_chain->run('example', $context));

        //Ensures the chain returns at the break condition as the example command returns false (the break condition)
        $this->_chain->enqueue($this->_command);
        $this->assertFalse($this->_chain->run('example', $context));
    }

    /**
     * Enable the chain
     *
     * @return  void
     */
    public function testEnable()
    {
        $context = new KCommandContext();

        $this->assertFalse($this->_chain->run('example', $context));

        $this->_chain->disable();
        $this->assertNull($this->_chain->run('example', $context));
    }

    /**
     * Disable the chain
     *
     * If the chain is disabled running the chain will always return TRUE
     *
     * @return  void
     */
    public function testDisable()
    {
        $context = new KCommandContext();

        $this->_chain->disable();
        $this->assertNull($this->_chain->run('example', $context));
    }

    /**
     * Set the priority of a command
     *
     * @param KCommandInterface $object
     * @param integer           $priority
     * @return KCommandChain
     * @throws \InvalidArgumentException if the object doesn't implement KCommandInterface
     */
    public function testSetPriority()
    {
        $this->_chain->setPriority($this->_command, KCommand::PRIORITY_HIGH);
        $this->assertTrue($this->_chain->getPriority($this->_command) == KCommand::PRIORITY_HIGH);
    }

    /**
     * Get the priority of a command
     *
     * @param  KCommandInterface $object
     * @return integer The command priority
     * @throws \InvalidArgumentException if the object doesn't implement KCommandInterface
     */
    public function testGetPriority()
    {
        $this->assertTrue($this->_chain->getPriority($this->_command) == KCommand::PRIORITY_NORMAL);
    }

    /**
     * Factory method for a command context.
     *
     * @return  KCommandContext
     */
    public function testGetContext()
    {
        $this->assertInstanceOf('KCommandContext', $this->_chain->getContext());
    }

    /**
     * Get the chain object stack
     *
     * @return     KObjectStack
     */
    public function testGetStack()
    {
        $this->assertInstanceOf('KObjectStack', $this->_chain->getStack());
    }
}
