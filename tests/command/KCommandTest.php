<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 26/02/2013
 * Time: 15:16
 */

class KCommandTest extends TestPhpunitCase
{
    /**
     * Generic Command handler
     *
     * @param 	string 	The command name
     * @param 	object  The command context
     * @return	boolean
     */
    public function testExecute()
    {
        $command = $this->getService('test:command.example');

        $this->assertFalse($command->execute('example', new KCommandContext()));
    }

    /**
     * Get the priority of the command
     *
     * @return	integer The command priority
     */
    public function testGetPriority()
    {
        $command = $this->getService('test:command.example');

        $this->assertNotNull($command->getPriority());
        $this->assertTrue(is_numeric($command->getPriority()));
    }
}
