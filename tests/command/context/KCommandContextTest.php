<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 26/02/2013
 * Time: 15:16
 */

class KCommandContextTest extends TestPhpunitCase
{
    /**
     * Get the command subject
     *
     * @return KObjectServiceable The command subject
     */
    public function testGetSubject()
    {
        $subject = $this->getService('lib:object');
        $context = new KCommandContext();
        $context->setSubject($subject);

        $this->assertEquals($context->getSubject(), $subject);
    }

    /**
     * Set the command subject
     *
     * @param KObjectServiceable $subject The command subject
     * @return KCommandContextInterface
     */
    public function testSetSubject()
    {
        $subject = $this->getService('lib:object');
        $context = new KCommandContext();
        $context->setSubject($subject);

        $this->assertEquals($context->getSubject(), $subject);
    }
}
